var restaurants = require('../controllers/restaurants.js');
var path = require('path');
module.exports = function (app) {
    app.post('/api/restaurants', restaurants.create); // create restaurants on post
    app.get('/api/restaurants', restaurants.index); // get all restaurants on get
    app.get('/api/restaurants/:id', restaurants.show); // show one restaurants on get
    app.put('/api/restaurants/edit/:id', restaurants.edit); // update one restaurants
    app.delete('/api/restaurants/:id', restaurants.destroy); // delete one restaurants on post
    app.all("*", (req, res, next) => {
        res.sendFile(path.resolve("./public/dist/public/index.html"));
    });
}