var mongoose = require('mongoose');
var Restaurant = mongoose.model('Restaurant');

module.exports = {
    //create Restaurant function for database
    create: function(req,res){
        console.log('hit create');
        var newRestaurant = new Restaurant(req.body);
        newRestaurant.save(function(err) {
            if(err){
                console.log('got errors');
                res.json(err);
            }else{
                console.log('success!');
                res.json(newRestaurant);
            }
        })
    },
    //end create Restaurant function

    //list restaurants function for database
    index: function(req,res){
        console.log('hit index');
        Restaurant.find({}, function(err, restaurants){
            res.json(restaurants);
        })
    },
    // end list function


    //show Restaurant function for database
    show: function(req,res){
        console.log('hit index');
        Restaurant.findOne({_id: req.params.id}, function(err, Restaurant){
            res.json(Restaurant);
        })
    },
    // end show Restaurant function


    // // edit Restaurant function for database
    edit: function(req,res){
        console.log('hit edit Restaurant');
        Restaurant.findOne({_id: req.params.id}, function(err,Restaurant){
            if(err){
                console.log('got errors on edit');
                res.json(err);
            }else{
                console.log('edited success!');
                Restaurant.title = req.body.title;
                Restaurant.description = req.body.description;
                Restaurant.location = req.body.location;
                Restaurant.phone = req.body.phone;
                Restaurant.imageUrl = req.body.imageUrl;
                Restaurant.save()
                res.json({message:"updated Restaurant"});
            }
        });
    },
    //end edit Restaurant function

    //begin delete Restaurant function for database
    destroy: function(req, res){
        console.log('hit delete Restaurant', req)
        Restaurant.remove({_id: req.params.id}, function(err){
            if(err){
                console.log("Error in destroy", err);
                res.json(err);
            } else {
                console.log("successful delete");
                res.json(true);
            }
        })
    },
    // end delete Restaurant function

}