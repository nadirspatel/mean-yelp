var mongoose = require('mongoose');
var RestaurantSchema = new mongoose.Schema({
    title: {type: String, required:true, minlength: 4},
    description: {type: String, required:true, minlength: 4},
    location: {type: String, required:true, minlength: 4},
    phone: {type: String, required:true, minlength: 4},
    imageUrl: {type: String}
}, { timestamps: true });

mongoose.model('Restaurant', RestaurantSchema);