import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private _http: HttpClient) { }
  postRestaurant(restaurantObj) {
    // actual post request happens here from form
    console.log('postRestaurant, restaurantObj is:', restaurantObj);
    return this._http.post('/api/restaurants', restaurantObj);
  }
  getAllRestaurants() {
    console.log('getAllRestaurants hit');
    return this._http.get('/api/restaurants');
  }
  getRestaurantbyID(id) {
    console.log('getRestaurantbyID hit');
    return this._http.get('/api/restaurants/' + id);
  }
  editRestaurant(restaurantObj, objId) {
    console.log('updateRestaurantbyID hit', restaurantObj, objId);
    return this._http.put('/api/restaurants/edit/' + objId, restaurantObj);
  }
  deleteRestaurant(objId) {
    console.log('deleteRestaurant hit', objId);
    return this._http.delete('/api/restaurants/' + objId);
  }

}
