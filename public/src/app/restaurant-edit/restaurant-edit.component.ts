import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-restaurant-edit',
  templateUrl: './restaurant-edit.component.html',
  styleUrls: ['./restaurant-edit.component.css']
})
export class RestaurantEditComponent implements OnInit {
  restaurant = {title: '', description: '', location: '', phone: '', imageUrl: '' };
  constructor(private _httpService: HttpService, private _router: Router, private _route: ActivatedRoute) { }

  ngOnInit() {
    this._route.params.subscribe((params: Params) => {
    console.log(params['id']);
    let tempObservable = this._httpService.getRestaurantbyID(params['id']);
    tempObservable.subscribe((data: any) => {
      this.restaurant = data;
    });
    });
  }

  // begin edit restaurant
  editRestaurant() {
    this._route.params.subscribe((params: Params) => {
    console.log('editRestaurant');
    // use the service to make a post request to the backend express server
    let tempObservable = this._httpService.editRestaurant(this.restaurant, params['id']);
    tempObservable.subscribe((data: any) => {
      console.log('got a response for edit restaurant', data);
      // any errors?
      if (!data.errors) {
        this._router.navigate(['/restaurants']);
      }
    });
  });
  }
  // end edit restaurant
}
