import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-restaurant-create',
  templateUrl: './restaurant-create.component.html',
  styleUrls: ['./restaurant-create.component.css']
})
export class RestaurantCreateComponent implements OnInit {
  newRestaurant = {title: '', description: '', location: '', phone: '', imageUrl: ''};
  constructor(private _httpService: HttpService, private _router: Router) { }

  ngOnInit() {
  }
  createRestaurant() {
    console.log('createRestaurant');
    // use the service to make a post request to the backend express server
    let tempObservable = this._httpService.postRestaurant(this.newRestaurant);
    tempObservable.subscribe((data: any) => {
      console.log('got a response', data);
      this.newRestaurant = {title: '', description: '', location: '', phone: '', imageUrl: ''};
      // any errors?
      if (!data.errors) {
        this._router.navigate(['/restaurants']);
      }
    });
  }
}
