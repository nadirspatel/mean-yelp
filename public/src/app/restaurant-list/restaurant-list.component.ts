import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.css']
})
export class RestaurantListComponent implements OnInit {
  restaurants = []; // now restaurants is defined for restaurant-list-component.html
  constructor(private _httpService: HttpService) { }

  ngOnInit() {
    // make a get request for all restaurants in database
    let tempObservable = this._httpService.getAllRestaurants();
    tempObservable.subscribe((data: any) => {
      console.log('got a reponse', data);
      this.restaurants = data;
    });
  }

  delete(objId){
    console.log('deleting record');
    let tempObservable = this._httpService.deleteRestaurant(objId);
    tempObservable.subscribe((data: any) => {
      console.log('record deleted successfully');
      if (!data.err) {
        this.ngOnInit();
      }
    });
  }

}
